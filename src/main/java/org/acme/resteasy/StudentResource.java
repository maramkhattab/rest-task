package org.acme.resteasy;
import Model.Student;
import Service.StudentService;
import Interface.StudentResourceInterface;
import javax.annotation.Generated;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


public class StudentResource implements StudentResourceInterface{
    StudentService studentService=new StudentService();


    public List<Student>  getStudents(@QueryParam("year") int year,
                                      @QueryParam("start") int start,
                                      @QueryParam("size") int size,
                                      @QueryParam("major") String major){
        if (year>0)
            return studentService.getStudentsForYear(year);
        if (start>0 && size>0)
            return studentService.getStudentsPaginated(start,size);
        if (major!=null && major.length()>0)
            return studentService.getStudentsForMajor(major);
        return studentService.getAllStudents();
    }


    public Student getStudent(@PathParam("studentId") long studentId){
        return studentService.getStudent(studentId);
    }

    public Response createStudent(Student student, @Context UriInfo uriInfo) throws URISyntaxException {
       Student newStudent=studentService.addStudent(student);
       URI uri=uriInfo.getAbsolutePathBuilder().path(String.valueOf(newStudent.getID())).build();
       return Response.created(uri)
            .entity(newStudent)
            .build();

    }


    public Student updateStudent(@PathParam("studentId") long studentId, Student student){
        student.setID(studentId);
        return studentService.updateStudent(student);
    }


    public void  deleteStudent(@PathParam("studentId") long studentId){

       studentService.removeStudent(studentId);
    }


}
