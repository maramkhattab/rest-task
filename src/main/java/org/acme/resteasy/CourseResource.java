package org.acme.resteasy;

import Model.Course;

import Service.CourseService;
import Interface.CourseResourceInterface;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


public class CourseResource implements CourseResourceInterface {
    CourseService courseService =new CourseService();


    public List<Course> getCourses(@QueryParam("subject") String subject,
                                   @QueryParam("name") String name,
                                   @QueryParam("start") int start,
                                   @QueryParam("size") int size){
        if(subject!=null && subject.length()>0)
            return courseService.getCoursesForSubject(subject);
        if(name!=null && name.length()>0)
            return courseService.getCoursesForName(name);

        if (start>0 && size>0)
            return courseService.getCoursesPaginated(start,size);
        return courseService.getAllCourses();
    }


    public Course getCourse(@PathParam("courseId") long courseId){
        return courseService.getCourse(courseId);
    }


    public Response createCourse(Course course, @Context UriInfo uriInfo) throws URISyntaxException {
        Course newCourse= courseService.addCourse(course);
        URI uri=uriInfo.getAbsolutePathBuilder().path(String.valueOf(newCourse.getID())).build();
        return Response.created(uri)
                .entity(newCourse)
                .build();
    }


    public Course updateStudent(@PathParam("courseId") long courseId, Course course){
        course.setID(courseId);
        return courseService.updateCourse(course);
    }


    public void  deleteCourse(@PathParam("courseId") long courseId){

        courseService.removeCourse(courseId);
    }




}
