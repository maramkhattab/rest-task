package Interface;

import Model.Student;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;
import java.util.List;

@Path("/students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)

public interface StudentResourceInterface {
    @GET
    public List<Student> getStudents(@QueryParam("year") int year,
                                     @QueryParam("start") int start,
                                     @QueryParam("size") int size,
                                     @QueryParam("major") String major);
    @GET
    @Path("/{studentId}")
    public Student getStudent(@PathParam("studentId") long studentId);

    @POST
    public Response createStudent(Student student, @Context UriInfo uriInfo) throws URISyntaxException;

    @PUT
    @Path("/{studentId}")
    public Student updateStudent(@PathParam("studentId") long studentId, Student student);

    @DELETE
    @Path("/{studentId}")
    public void  deleteStudent(@PathParam("studentId") long studentId);
}
