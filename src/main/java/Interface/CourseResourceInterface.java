package Interface;

import Model.Course;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;
import java.util.List;

@Path("/courses")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface CourseResourceInterface {
    @GET
    public List<Course> getCourses(@QueryParam("subject") String subject,@QueryParam("name") String name, @QueryParam("start") int start,
            @QueryParam("size") int size);

    @GET
    @Path("/{courseId}")
    public Course getCourse(@PathParam("courseId") long courseId);

    @POST
    public Response createCourse(Course course, @Context UriInfo uriInfo) throws URISyntaxException;

    @PUT
    @Path("/{courseId}")
    public Course updateStudent(@PathParam("courseId") long courseId, Course course);

    @DELETE
    @Path("/{courseId}")
    public void deleteCourse(@PathParam("courseId") long courseId);

}
