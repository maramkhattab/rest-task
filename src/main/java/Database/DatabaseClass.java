package Database;

import Model.Course;
import Model.Student;

import java.util.HashMap;
import java.util.Map;

public class DatabaseClass {
    public static Map<Long, Student> students=new HashMap<>();


    public static Map<Long, Course> courses=new HashMap<>();

    public static Map<Long, Student>  getStudents(){
        return students;
    }
    public static Map<Long, Course>  getCourses(){
        return courses;
    }
}
