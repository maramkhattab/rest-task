package Model;

import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
public class Student {
    private long ID;
    private String name;
    private long enrollmentYear;
    private String major;
    public Student(){

    }
    public Student(String name, long enrollmentYear, String major) {

        this.name = name;
        this.enrollmentYear = enrollmentYear;
        this.major = major;
    }

    public Student(long ID, String name, long enrollmentYear, String major) {
        this.ID = ID;
        this.name = name;
        this.enrollmentYear = enrollmentYear;
        this.major = major;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getEnrollmentYear() {
        return enrollmentYear;
    }

    public void setEnrollmentYear(long enrollmentYear) {
        this.enrollmentYear = enrollmentYear;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
}
