package Model;

import java.util.List;

public class Course {
    private long ID;
    private String name;
    private List<Student> students;

    public Course(){

    }

    public Course(long ID, String name, List<Student> students) {
        this.ID = ID;
        this.name = name;
        this.students = students;
    }



    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
