package Exception;

import Model.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {
    @Override
    public Response toResponse (DataNotFoundException ex){
        ErrorMessage errorMessage= new ErrorMessage(ex.getMessage(),404,"google.com");
       return Response.status(Response.Status.NOT_FOUND)
       .entity(errorMessage)
       .build();
    }

}