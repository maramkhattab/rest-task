package Service;

import Database.DatabaseClass;
import Model.Course;
import Model.Student;
import Exception.DataNotFoundException;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CourseService {
    private Map<Long, Course> courses = DatabaseClass.getCourses();

    public CourseService() {
        StudentService studentService= new StudentService();
        courses.put(1L, new Course(1, "English", studentService.getStudentsForMajor("business")));
        courses.put(2L, new Course(2, "Math",studentService.getAllStudents()));
        courses.put(3L, new Course(3, "calculus", studentService.getStudentsForMajor("engineering")));

    }

    public List<Course> getAllCourses() {
        return new ArrayList<Course>(courses.values());
    }

    public List<Course> getCoursesForSubject(String subject) {
        return courses.values().stream().filter(course -> course.getName().contains(subject)).collect(Collectors.toList());
    }
    public List<Course> getCoursesForName(String name) {
        return courses.values().stream().filter(course -> course.getName().equalsIgnoreCase(name)).collect(Collectors.toList());
    }


    public List<Course> getCoursesPaginated(int start, int size) {
        ArrayList<Course> list = new ArrayList<Course>(courses.values());
        if (start + size > list.size())
            return new ArrayList<Course>();
        return list.subList(start, start + size);
    }

    public Course getCourse(long ID) {
        Course course = courses.get(ID);
        if (course == null)
        {
            throw new DataNotFoundException("Course with ID "+ID+"is not found");
        }

        return course;
    }

    public Course addCourse(Course course) {
        course.setID(courses.size() + 1);
        courses.put(course.getID(), course);
        return course;
    }

    public Course updateCourse(Course course) {
        if (course.getID() <= 0)
            return null;
        courses.put(course.getID(), course);
        return course;
    }

    public Course removeCourse(long ID) {
        return courses.remove(ID);
    }
}
