package Service;

import Database.DatabaseClass;
import Model.Student;
import Exception.DataNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentService {

    private Map<Long, Student> students= DatabaseClass.getStudents();
    public StudentService(){
        students.put(1L ,new Student(1,"Hana khaled",2018,"Engineering"));
        students.put(2L ,new Student(2,"Zeina Ashraf",2018,"MassComm"));
        students.put(3L,new Student(3,"Aly Mourad",2019,"Business"));
        students.put(4L,new Student(4,"May Aly",2017,"Business"));
    }

    public List<Student> getAllStudents(){
       return new ArrayList<Student>(students.values()) ;
    }
    public  Student getStudent(long ID){
    	Student student=students.get(ID);
    	if (student==null)
    		throw new DataNotFoundException("Student with ID "+ID+" not found");
        return student;
    }

    public List<Student> getStudentsForYear(long year){
        return students.values().stream().filter(student-> student.getEnrollmentYear()==year).collect(Collectors.toList());
    }
    public List<Student> getStudentsForMajor(String major){
        return students.values().stream().filter(student-> student.getMajor().equalsIgnoreCase(major)).collect(Collectors.toList());
    }
    public List<Student> getStudentsPaginated(int start, int size){
        ArrayList<Student> list= new ArrayList<Student>(students.values());
        if (start+size> list.size())
            return new ArrayList<Student>();
        return list.subList(start,start+size);
    }

    public Student addStudent(Student student){
        student.setID(students.size()+1);
        students.put(student.getID(),student);
        return student;
    }
    public Student updateStudent(Student student){
        if (student.getID()<=0)
            return null;
        students.put(student.getID(),student);
        return student;
    }
    public Student removeStudent(long ID){
        return students.remove(ID);
    }
}
